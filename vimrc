if filereadable(expand("~/.vimrc.before"))
  source ~/.vimrc.before
endif

" Line numbers
set number
" No .swp file please
set noswapfile
" Highlight search matches
set hlsearch
" Stop search wraparound
set nowrapscan

if filereadable(expand("~/.vimrc.after"))
  source ~/.vimrc.after
endif
